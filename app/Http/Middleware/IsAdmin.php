<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
            $current_user_role = Auth::user()->role;
            $is_admin = $current_user_role === 'admin';
            if ($is_admin) {
                return $next($request);
            }    
        }
           
        return redirect('/home');
    }
}
