<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_status;
use Illuminate\Support\Facades\Auth;


class UserStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_user_status = $request->all();
        if (Auth::user()) {
            $current_user = Auth::user()->id;
            $new_user_status['added_by'] = $current_user;
        }
        if (Auth::user() && Auth::user()->role === "admin") {
            try {
                User_status::create($new_user_status);
                return redirect('/admin/user/'.$request->all()['user_id']);
            } catch (\Exception $e){
                $errorCode = $e->errorInfo[1];
                $data['error_message'] = $errorCode . ' Ошибка! Попробуйте ещё раз.';
                return view('/admin/user/create', $data);
            }         
        } else {
            // Auth::user()->id;
            if (Auth::user()->id == $request->all()['user_id']) {
                try {
                    User_status::create($new_user_status);
                    return redirect('/home');
                } catch (\Exception $e){
                    $errorCode = $e->errorInfo[1];
                    $data['error_message'] = $errorCode . ' Ошибка! Попробуйте ещё раз.';
                    return view('/home', $data);
                }   
            } else {
                return redirect('/home');            
            }
        }













        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
