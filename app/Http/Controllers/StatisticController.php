<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\User_comments;

use Illuminate\Support\Facades\DB;


class StatisticController extends Controller
{
    
    public function index(Request $request)
    {
        $users = $this->all_users($request);
        $users = $users->with('user_statuses')->get();
        $row_colors = array_map( array($this, 'genereteRowColor') , $users->all());
        $user_status_texts = array_map( array($this, 'genereteUserStatusText') , $users->all());
        
        $data = [
          'users'=> $users,
          'filter_name_ru'=>'Все пациенты',
          'filter_name' =>'all_users',
          'row_colors'=>$row_colors,
          'user_status_texts' => $user_status_texts
        ];
        return view('admin/stat/index', $data);
    }

    private function getFilterName($method) {
      $filterName = config('constants.filter_names.'.$method);
      return $filterName ? $filterName : $method;
    }
    
    public function getStat(Request $request)
    {
        $method = $request->all()['filter_name'];
        $oldData = $request->all();
        $users =$this->{$method}($request);
        $users = $users->with('user_statuses')->get();
        $row_colors = array_map( array($this, 'genereteRowColor'), $users->all());
        $user_status_texts = array_map( array($this, 'genereteUserStatusText') , $users->all());
        
        $data = [
          'users'=> $users,
          'filter_name_ru'=>$this->getFilterName($method),
          'filter_name' => $method,
          'oldData'=>$oldData,
          'row_colors'=>$row_colors,
          'user_status_texts' => $user_status_texts
        ];
        return view('admin/stat/index', $data);
    }

    private function createTableForExport($users) {
      foreach ($users->all() as $user) {
        $user_data = $user->getAttributes();
        $statuses = $user->user_statuses->all();
        if (count($statuses) > 0) {
          $user_status = $statuses[count($statuses)-1]->getAttributes();
        }
      }
      
    }

    private function all_users(Request $request) {
      return User::select(
        'id',
        'last_name',
        'name',
        'second_name',
        'birth_day',
        'phone_number',
        'arrival_date',
        'current_address',
        'hospital_number',
        'arrival_country',
        'quarantine_end_date',
        'take_1',
        'take_1_comment',
        'take_1_result',
        'take_2',
        'take_2_comment',
        'take_2_result',
        'take_3',
        'take_3_comment',
        'take_3_result',
        'is_need_bl',
        'bl_receive_date',
        'child_qty',
        'job',
        'control_end_date',
        'control_result',
        'isolation_conditions',
        'email'
      )->where('role', 'user');
      
    }

    private function users_under_contriol( Request $request) { 
      $users = $this->all_users($request);
      return $users->where('control_end_date', null);
    }

    private function covid19( Request $request) { 
      $users = $this->all_users($request);
      return $users->where('take_1_result', '=', 1)->orWhere('take_2_result', '=', 1)->orWhere('take_3_result', '=', 1);
    }

    

    private function covid19_under_control( Request $request) { 
      $users = $this->all_users($request);
      return $users->where(function ($query) {
        $query->where('take_1_result', '=', 1)->orWhere('take_2_result', '=', 1)->orWhere('take_3_result', '=', 1);
      })->where('control_end_date', null);
    }
    
    private function not_under_control( Request $request) { 

      $users = $this->all_users($request)->where('control_end_date', null)->whereHas('user_statuses', function ($query) {
        $query
        ->where('temperature', '>', '36.9')
        ->orWhere('cough_pain_throat', '=', 1)
        ->orWhere('short_wind', '=', 1)
        ->orWhere('need_consult', '=', 1);
    });
      return $users;
    }

    private function contacted_by_date(Request $request) { 
      $filterData = $request->all();
      $users = $this->all_users($request)->whereBetween('arrival_date', array($filterData['contacted_by_date_from'], $filterData['contacted_by_date_to']? $filterData['contacted_by_date_to'] : \Carbon\Carbon::now()  ));
      return $users;
    }

    private function users_under_contriol_grouped_by_isilation_condition(Request $request) { 
      $filterData = $request->all();
      $date = $filterData['users_under_contriol_grouped_by_isilation_condition_date'];
      $users = $this->all_users($request)->select(DB::raw('count(*) as user_count, isolation_conditions'))
      ->where(function ($query) use($date) {
      $query->where('control_end_date', null)
            ->orWhere('control_end_date', '<', $date);
    })->groupBy('isolation_conditions');
      return $users;
    }

    

    private function users_grouped_by_isilation_condition(Request $request) { 
      $users = $this->all_users($request)->where('control_end_date', null)->select(DB::raw('count(*) as user_count, isolation_conditions')) ->groupBy('isolation_conditions');
      return $users;
    }

    private function biomaterial_sampling_required(Request $request) { 
      $date = $request->all()['biomaterial_sampling_required_date'];
      $users = $this->all_users($request)
      ->where(function ($query) use($date) {
        $query->where('control_end_date', null)
              ->where('role', 'user')
              ->whereDate('take_1','=', $date)
              ->where('take_1_comment', null);
      })
      ->orWhere(function ($query) use($date) {
        $query->where('control_end_date', null)
              ->where('role', 'user')
              ->whereDate('take_2','=', $date)
              ->where('take_2_comment', null);
      })
      ->orWhere(function ($query) use($date) {
        $query->where('control_end_date', null)
              ->where('role', 'user')
              ->whereDate('take_3','=', $date)
              ->where('take_3_comment', null);
      });
      return $users;
    }

    private function quaratine_end_by_date(Request $request) { 
      $filterData = $request->all();
      $users = $this->all_users($request)->whereBetween('control_end_date', array($filterData['quaratine_end_by_date_from'], $filterData['quaratine_end_by_date_to']? $filterData['quaratine_end_by_date_to'] : \Carbon\Carbon::now()  ));
      return $users;
    }

    

    private function created_users_by_date(Request $request) { 
      $filterData = $request->all();
      $users = $this->all_users($request)->where('control_end_date', null)->whereDate('created_at','=', $filterData['created_users_by_date_date']);
      return $users;
    }

    
    

    private function take_by_date(Request $request) { 
      $filterData = $request->all();
      $users = $this->all_users($request)->where('take_1','=', $filterData['take_by_date_date'])->orWhere('take_2','=', $filterData['take_by_date_date'])->orWhere('take_3','=', $filterData['take_by_date_date']);
      return $users;
    }


    

    private function personal_data_is_not_filled(Request $request) { 
      $filterData = $request->all();
      $users = $this->all_users($request)
      ->where(function ($query) {
        $query->where('name','=', null)
        ->orWhere('second_name','=', null)
        ->orWhere('birth_day','=', null)
        ->orWhere('phone_number','=', null)
        ->orWhere('arrival_date','=', null)
        ->orWhere('current_address','=', null);
      }
      );
      
      return $users;
    }
    
    private function genereteRowColor($user) {
      $users_data = $user->getAttributes();
      $user_statuses = $user->user_statuses->all();
      if (!array_key_exists("control_end_date",$users_data)) {
        return '';
      }
      if ($users_data['control_end_date'] !== null) {
        return 'gray_row';
      }
      if ($users_data['take_1_result'] === 1 ||
          $users_data['take_1_result'] === 1 ||
          $users_data['take_1_result'] === 1 
          ) {
        return 'red_row';
      }
      if ($users_data['control_result'] === "Передан в другой район") {
        return 'blue_row';
      }
      // simpoms and user under control
      foreach ($user_statuses as $status_record) {
        $status_attributes = $status_record->getAttributes();
        if ($status_attributes['temperature']> 36.9 ||
        $status_attributes['cough_pain_throat'] === 1 ||
        $status_attributes['short_wind'] === 1 ||
        $status_attributes['need_consult'] === 1 ) {
          return 'orange_row';
        }  
      }
      // staus is not uo to date on user under control
      foreach ($user_statuses as $status_record) {
        $status_attributes = $status_record->getAttributes();
        if (\Carbon\Carbon::parse($status_attributes['date'])->format('Y-m-d') === \Carbon\Carbon::now()->format('Y-m-d')) {
          return '';
        };
      }
      return 'yellow_row';
    }


    private function genereteUserStatusText($user) {
      $users_data = $user->getAttributes();
      $user_statuses = $user->user_statuses->all();
      
      if (count($user_statuses)>0) {
        // $status_attributes = $status_record->getAttributes();
        $last_status = $user_statuses[count($user_statuses)-1]->getAttributes();
        $text = '';
        $text .= $last_status['date'] . "\n";
        if ($last_status['temperature'] !== null) {
          $text .=  "Температура: ". $last_status['temperature'] . "\n";
        }
        if ($last_status['cough_pain_throat'] !== null && $last_status['cough_pain_throat'] === 1) {
          $text .=  "Кашель/Боль в горле: ". $last_status['cough_pain_throat'] . "\n";
        }
        if ($last_status['short_wind'] !== null && $last_status['short_wind'] === 1) {
          $text .=  "Одышка: ". $last_status['short_wind'] . "\n";
        }
        if ($last_status['need_consult'] !== null && $last_status['need_consult'] === 1) {
          $text .=  "Иные симптомы/ Хочет консультацию врача: ". $last_status['need_consult'] . "\n";
        }
        if ($last_status['comment'] !== null) {
          $text .=  "Комментарий: ". $last_status['comment'] . "\n";
        }
        if ($last_status['user_id'] !== $last_status['added_by']) {
          $text .=  "Добавлено Админом"."\n";
        } else {
          $text .=  "Добавлено Пользователем\n";
        }
        return $text;
      }
     
      return 'Hет данных';
    }
    
    // default filter is all_users
    public function __call($methodName, $args) {
      return $this->all_users($args[0]);
    }
}
