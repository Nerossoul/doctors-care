<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            $users = User::select(
                'id',
                'last_name',
                'name',
                'second_name',
                'birth_day',
                'phone_number',
                'arrival_date',
                'current_address',
                'hospital_number',
                'arrival_country',
                'quarantine_end_date',
                'take_1',
                'take_1_comment',
                'take_1_result',
                'take_2',
                'take_2_comment',
                'take_2_result',
                'take_3',
                'take_3_comment',
                'take_3_result',
                'is_need_bl',
                'bl_receive_date',
                'child_qty',
                'job',
                'control_end_date',
                'control_result',
                'isolation_conditions',
                'email'
            )->where('role', 'user')->get();

            $data = ['users' => $users];
            return view('admin/user/index', $data);
        } else {

            return redirect('/home');
        }

    }

    public function search(Request $request)
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            $hospital_number = $request->all()['hospital_number'];
            $search_params = $request->all();
            $users = User::select(
                'id',
                'last_name',
                'name',
                'second_name',
                'birth_day',
                'email',
                'phone_number',
                'arrival_date',
                'current_address',
                'hospital_number',
                'arrival_country',
                'quarantine_end_date',
                'take_1',
                'take_1_comment',
                'take_1_result',
                'take_2',
                'take_2_comment',
                'take_2_result',
                'take_3',
                'take_3_comment',
                'take_3_result',
                'is_need_bl',
                'bl_receive_date',
                'child_qty',
                'job',
                'control_end_date',
                'control_result',
                'isolation_conditions'
            )->where('role', 'user');
            if (isset($search_params['last_name'])) {
                $users = $users->where('last_name', 'LIKE', '%' . $search_params['last_name'] . '%');
            }
            if (isset($search_params['hospital_number'])) {
                $users = $users->where('hospital_number', $hospital_number);
            }
            if (isset($search_params['users_under_control'])) {
                $users = $users->where('control_end_date', null);
            }

            $users = $users->get();
            $data = ['users' => $users, 'search_params' => $search_params];
            return view('admin/user/index', $data);
        } else {

            return redirect('/home');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            return view('admin/user/create');
        } else {

            return redirect('/home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            $data = $request->all();
            $newUser = [];
            if (isset($data['last_name'])) {
                $newUser['last_name'] = $data['last_name'];
            }

            if (isset($data['name'])) {
                $newUser['name'] = $data['name'];
            }
            if (isset($data['second_name'])) {
                $newUser['second_name'] = $data['second_name'];
            }
            if (isset($data['birth_day'])) {
                $newUser['birth_day'] = $data['birth_day'];
            }

            if (isset($data['phone_number'])) {
                $newUser['phone_number'] = $data['phone_number'];
            }

            if (isset($data['arrival_date'])) {
                $newUser['arrival_date'] = $data['arrival_date'];
            }

            if (isset($data['current_address'])) {
                $newUser['current_address'] = $data['current_address'];
            }

            if (isset($data['hospital_number'])) {
                $newUser['hospital_number'] = $data['hospital_number'];
            }

            if (isset($data['current_address'])) {
                $newUser['current_address'] = $data['current_address'];
            }

            if (isset($data['arrival_country'])) {
                $newUser['arrival_country'] = $data['arrival_country'];
            }

            if (isset($data['quarantine_end_date'])) {
                $newUser['quarantine_end_date'] = $data['quarantine_end_date'];
            }

            if (isset($data['take_1'])) {
                $newUser['take_1'] = $data['take_1'];
            }

            if (isset($data['take_1_result'])) {
                $newUser['take_1_result'] = $data['take_1_result'];
            }

            if (isset($data['take_2'])) {
                $newUser['take_2'] = $data['take_2'];
            }

            if (isset($data['take_2_result'])) {
                $newUser['take_2_result'] = $data['take_2_result'];
            }

            if (isset($data['take_3'])) {
                $newUser['take_3'] = $data['take_3'];
            }

            if (isset($data['take_3_result'])) {
                $newUser['take_3_result'] = $data['take_3_result'];
            }

            if (isset($data['is_need_bl'])) {
                $newUser['is_need_bl'] = $data['is_need_bl'];
            }

            if (isset($data['bl_receive_date'])) {
                $newUser['bl_receive_date'] = $data['bl_receive_date'];
            }

            if (isset($data['child_qty'])) {
                $newUser['child_qty'] = $data['child_qty'];
            }

            if (isset($data['job'])) {
                $newUser['job'] = $data['job'];
            }

            if (isset($data['control_end_date'])) {
                $newUser['control_end_date'] = $data['control_end_date'];
            }

            if (isset($data['isolation_conditions'])) {
                $newUser['isolation_conditions'] = $data['isolation_conditions'];
            }
            // create temp password

            $tempPassword = uniqid();
            $tempEmail = $tempPassword . "@stay_at_home.ru";
            $newUser['email'] = $tempEmail;
            $newUser['password'] = Hash::make($tempPassword);

            // ;
            try {
                User::create($newUser);
                return redirect('/admin/user');
            } catch (\Exception $e) {
                $errorCode = $e->errorInfo[1];
                if ($errorCode == 1062) {
                    $data['error_message'] = 'Такой телефон уже привязан другому пациенту';
                }
                $data['error_message'] = 'Ошибка! Попробуйте ещё раз.';
                return view('/admin/user/create', $data);
            }
        } else {
            return redirect('/home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            $user_and_statuses = User::with('user_statuses')->where('id', $user->id)->get();
            $user_and_telemedicine = User::with('telemedicines')->where('id', $user->id)->get();
            $user->statuses = $user_and_statuses->flatMap->user_statuses;
            $user->telemedicines = $user_and_telemedicine->flatMap->telemedicines;
            return view('admin/user/show', $user);
        } else {
            return redirect('/home');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            return view("admin/user/edit", $user);
        } else {
            // Auth::user()->id;
            if (Auth::user()->id == $user->id) {
                return view("user/edit", $user);
            } else {
                return redirect('/home');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        if (Auth::user() && Auth::user()->role === "admin") {
            try {
                $user->update($request->all());
                return redirect("admin/user/" . $user->id);
            } catch (\Exception $e) {
                $errorCode = $e->errorInfo[1];
                if ($errorCode == 1062) {
                    $data['error_message'] = 'Такой телефон уже привязан другому пациенту';
                }
                $data['error_message'] = 'Ошибка! Попробуйте ещё раз.';
                return redirect("admin/user/" . $user->id . '/edit');
            }
        } else {
            // Auth::user()->id;
            if (Auth::user()->id == $user->id) {
                try {
                    $user->update($request->all());
                    return redirect("/home");
                } catch (\Exception $e) {
                    $errorCode = $e->errorInfo[1];
                    if ($errorCode == 1062) {
                        $data['error_message'] = 'Такой телефон уже привязан другому пациенту';
                    }
                    $data['error_message'] = 'Ошибка! Попробуйте ещё раз.';
                    return redirect("admin/user/" . Auth::user()->id . '/edit');
                }
            } else {
                return redirect('/home');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            //
        } else {
            return redirect('/home');
        }
    }
}