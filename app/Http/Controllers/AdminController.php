<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\User_comments;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
            $users = User::all();
            foreach ($users as $user) {
                // dump($user->name);
            }
            $data = ['users'=> $users];
            return view('admin/admin', $data);
    }
}
