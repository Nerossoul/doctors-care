<?php

namespace App\Http\Controllers;

use App\Telemedicine;
use Illuminate\Http\Request;

class TelemedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $newUser = [];
        //validation
        if ($data["diagnosis_code"] === null &&
            $data["ext_diagnosis_code"] === null &&
            $data["tmc_date"] === null &&
            $data["consultant_name"] === null) {
            return redirect('/admin/user/' . $data['user_id']);
        }

        try {
            Telemedicine::create($data);
            return redirect('/admin/user/' . $data['user_id']);
        } catch (\Exception $e) {
            // $errorCode = $e->errorInfo[1];
            // $data['error_message'] = 'Ошибка: ' . $errorCode . '. Попробуйте ещё раз.';
            return redirect('/admin/user/' . $data['user_id']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}