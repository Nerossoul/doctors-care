<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\User_comments;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user() && Auth::user()->role === "admin") {
            return redirect('/admin');         
        } else {
            if (Auth::user()) {
                $user = User::find(Auth::user()->id);
                $user_and_statuses = User::with('user_statuses')->where('id', Auth::user()->id)->get();
                $user->statuses = $user_and_statuses->flatMap->user_statuses;
                return view('home', $user);
            } else {
                return redirect('/login');
            } 
        }
        
    }
}
