<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'last_name',
    'name',
    'second_name',
    'birth_day',
    'email',
    'phone_number',
    'arrival_date',
    'current_address',
    'hospital_number',
    'arrival_country',
    'quarantine_end_date',
    'take_1',
    'take_1_result',
    'take_2',
    'take_2_result',
    'take_3',
    'take_3_result',
    'take_1_comment',
    'take_2_comment',
    'take_3_comment',
    'is_need_bl',
    'bl_receive_date',
    'child_qty',
    'job',
    'control_end_date',
    'control_result',
    'isolation_conditions',
    'ext_information',
    'role',
    'email_verified_at',
    'password'
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_statuses()
    {
        return $this->hasMany('App\User_status');
    }

    public function telemedicines()
    {
        return $this->hasMany('App\Telemedicine');
    }
}