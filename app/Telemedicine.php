<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telemedicine extends Model
{
    protected $table = 'telemedicines';
    protected $fillable = [
        'id',
        'user_id',	
        'tmc_date',	
        'diagnosis_code',
        'ext_diagnosis_code',
        'consultant_name',
        'ext_information'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
