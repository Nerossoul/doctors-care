<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_status extends Model
{
    protected $table = 'user_statuses';
    protected $fillable = [
        'user_id', 'date', 'temperature', 'cough_pain_throat', 'need_consult', 'added_by', 'short_wind', 'comment', 'ext_information',
    ];

    public function isAdmin()
    {
        return true;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}