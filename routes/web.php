<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// users dashboard route
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/status', 'UserStatusController');

// doctors dashboard route
Route::get('/config_cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'config cache cleared' . $exitCode;
})->middleware('isAdmin');

Route::get('/migrate', function() {
    $exitCode = Artisan::call('migrate');
    return 'migration done' . $exitCode;
})->middleware('isAdmin');

Route::get('/admin', 'AdminController@index')->name('admin')->middleware('isAdmin');

Route::resource('/admin/user', 'AdminUserController');
Route::resource('/admin/tmc', 'TelemedicineController')->middleware('isAdmin');
Route::post('/admin/user/search', 'AdminUserController@search')->middleware('isAdmin')->name('search');

Route::get('/admin/stat', 'StatisticController@index')->middleware('isAdmin')->name('stat');
Route::post('/admin/stat', 'StatisticController@getStat')->middleware('isAdmin')->name('stat.data');