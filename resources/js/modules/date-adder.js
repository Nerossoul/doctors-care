(function() {
    let startDateInput = document.querySelector("#arrival_date");
    let endDateInput = document.querySelector("#quarantine_end_date");

    if (startDateInput !== null && endDateInput !== null) {
        let startDate = new Date(startDateInput.value);
        let endDate = new Date(endDateInput.value);
        function addDays(oldDate, days) {
            var date = new Date(oldDate.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }
        function addTwoWeeks() {
            let newEndDate = addDays(new Date(startDateInput.value), 13);
            let newMonth =
                (newEndDate.getMonth() + 1).toString().length < 2
                    ? "0" + (newEndDate.getMonth() + 1)
                    : newEndDate.getMonth() + 1;
            let newDayNumber =
                newEndDate.getDate().toString().length < 2
                    ? "0" + newEndDate.getDate()
                    : newEndDate.getDate();
            let newDateString =
                newEndDate.getFullYear() + "-" + newMonth + "-" + newDayNumber;
            endDateInput.value = newDateString;
            printDelta();
        }
        function countDateDelta(startDate, endDate) {
            return (
                1 + Math.floor((endDate - startDate) / (1000 * 60 * 60 * 24))
            );
        }
        function printDelta() {
            let dateDelta = document.querySelector("#dateDelta");
            if (dateDelta !== null) {
                if (startDateInput.value === "" || endDateInput.value === "") {
                    dateDelta.innerText = "Дата окончания карантина";
                } else {
                    let startDate = new Date(startDateInput.value);
                    let endDate = new Date(endDateInput.value);
                    dateDelta.innerText =
                        "Дата окончания карантина ( " +
                        countDateDelta(startDate, endDate) +
                        " )";
                }
            }
        }

        printDelta();
        startDateInput.addEventListener("change", printDelta);
        endDateInput.addEventListener("change", printDelta);
        document
            .querySelector("#addTwoWeeks")
            .addEventListener("click", addTwoWeeks);
    }
})();
