(function() {
    let filterCloseBtn = document.querySelector("#close_filter_btn");
    let filterOpenBtn = document.querySelector("#open_filter_btn");
    let filter = document.querySelector("#stat_filter");

    function hideStatFilter() {
        console.log("close");
        filter.classList.add("d-none");
        filterOpenBtn.classList.remove("d-none");
    }

    function showStatFilter() {
        filter.classList.remove("d-none");
        filterOpenBtn.classList.add("d-none");
    }
    if (filter !== null && filterCloseBtn !== null) {
        filterCloseBtn.addEventListener("click", hideStatFilter);
    }
    if (filterOpenBtn !== null) {
        filterOpenBtn.addEventListener("click", showStatFilter);
    }

})();
