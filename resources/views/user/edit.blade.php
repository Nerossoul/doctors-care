@extends('layouts.app')
@section('content')
<h2>{{$last_name ?? ''}} {{$name ?? ''}} {{$second_name ?? ''}}(Изменить профиль)</h2>
@if (isset($error_message))
<div class="alert alert-danger" role="alert">
    {{  $error_message }}
</div>
@endif
{{ Form::open(array('route' => ['user.update', $id], 'method' => 'put')) }}
<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('last_name', 'Фамилия (Обязательное поле)') }}
            {{ Form::text('last_name', $last_name ?? null, ['class'=>"form-control", 'placeholder'=>"Фамилия", 'required'=>'']) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('name', 'Имя (Обязательное поле)') }}
            {{ Form::text('name', $name ?? null, ['class'=>"form-control", 'placeholder'=>"Имя", 'required'=>'']) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('second_name', 'Отчество') }}
            {{ Form::text('second_name', $second_name ?? null, ['class'=>"form-control", 'placeholder'=>"Отчество"]) }}
        </div>
    </div>
    {{--
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('hospital_number', 'Номер отделения') }}
            {{ Form::number('hospital_number', $hospital_number ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
    --}}
</div>

<div class="row">
    <div class="col-sm " style='max-width:240px;'>
        <div class="form-group">
            {{ Form::label('birth_day', 'Дата рождения') }}
            {{ Form::date('birth_day', $birth_day ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('phone_number', 'Телефон') }}
            {{ Form::text('phone_number', $phone_number ?? null, ['class'=>"form-control", 'placeholder'=>"Телефон"]) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('current_address', 'Текущий Адрес') }}
            {{ Form::text('current_address', $current_address ?? null, ['class'=>"form-control", 'placeholder'=>"Введите адрес"]) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('child_qty', 'Кличество детей') }}
            {{ Form::number('child_qty', $child_qty ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('job', 'Место работы') }}
            {{ Form::text('job', $job ?? null, ['class'=>"form-control", 'placeholder'=>"Наименование предприятия"]) }}
        </div>
    </div>
</div>
{{-- 
<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('arrival_date', 'Дата прибытия') }}
            {{ Form::date('arrival_date', $arrival_date ?? null, ['class'=>"form-control", "id"=>"arrival_date"]) }}
        </div>
    </div>
    <div class="col-sm-1 pt-4">
        <button type="button" class="btn btn-primary btn-sm mb-1" id="addTwoWeeks">+ 14 дней</a>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('quarantine_end_date', 'Дата окончания карантина',['id'=>"dateDelta"]) }}
            {{ Form::date('quarantine_end_date', $quarantine_end_date ?? null, ['class'=>"form-control", 'id'=>"quarantine_end_date", "disabled"=>"disabled"]) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('arrival_country', 'Страна прибытия / контакт') }}
            {{ Form::text('arrival_country', $arrival_country ?? null, ['class'=>"form-control", 'placeholder'=>"arrival_country"]) }}
        </div>
    </div>
</div>
--}}
<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('is_need_bl', 'Нужен больничный?') }}
            {{ Form::select('is_need_bl', ['0' => 'Нет', '1' => 'Да'], $is_need_bl ?? '0', ['class'=>"form-control"]) }}
        </div>

    </div>
 {{--    
 <div class="col-sm">
        <div class="form-group">
            {{ Form::label('bl_receive_date', 'Дата выдачи больничного листа') }}
            {{ Form::date('bl_receive_date', $bl_receive_date ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
    --}}
</div>

<div class="form-group">
    {{ Form::label('isolation_conditions', 'Условия изоляции') }}
    {{ Form::select('isolation_conditions', config('constants.isolation_conditions'), $isolation_conditions ?? null, ['class'=>"form-control"]) }}
</div>
{{-- 
<div class="form-group">
    {{ Form::label('control_end_date', 'Дата окончания контроля') }}
    {{ Form::date('control_end_date', $control_end_date ?? null, ['class'=>"form-control"]) }}
</div>

<div class="form-group">
    {{ Form::label('control_result', 'Исход контроля') }}
    {{ Form::select('control_result', config('constants.control_result'), $control_result ?? null, ['class'=>"form-control"]) }}
</div>
--}}


<div class="form-group">
    {{ Form::submit('Сохранить' , ['class'=>'btn btn-primary']) }} 
    <a href="{{ route('home') }}" class="btn btn-secondary">Отмена</a>
</div>
{{ Form::close() }}
@endsection