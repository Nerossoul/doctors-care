@extends('layouts.app')
@section('content')
<div id="open_filter_btn" class="row ml-1 mr-1 d-none">
    <div class="col-sm-12 col-md-2">
        <button type="button" class="btn btn-primary btn-sm" aria-label="Close">
            <span aria-hidden="true">фильтр ></span>
        </button>
    </div>
</div>
<div class="row ml-1 mr-1 ">
    <div id="stat_filter" class="col-sm-12 col-md-4 " style="border: 1px solid #888;">
        <button id="close_filter_btn" type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h5>{{$filter_name_ru ?? ''}} - {{count($users)}} </h5>
        <div>
            {{ Form::open(array('route' => 'stat.data')) }}
            <div class="form-group">
                {{ Form::submit('Применить', ['class'=>'btn btn-primary btn-sm mr-2']) }}
                <a href=# onclick="exportToExcel('xlsx')">
                    <img width="30px" src="{{ asset('images/icons/excel_icon.svg') }}" alt="excel export">
                </a>
            </div>

            <hr>
            <div class="form-check">
                {{Form::radio('filter_name', 'all_users', $filter_name === 'all_users' ? true : false, ['class'=>"form-check-input"])}}
                {{ Form::label('isolation_conditions', 'Всего зарегистрировано', ['class'=>"form-check-label" , 'style'=>'color:green;']) }}
            </div>
            <hr>
            <div class="form-check">
                {{Form::radio('filter_name', 'users_under_contriol',  $filter_name === 'users_under_contriol' ? true : false, ['class'=>"form-check-input"])}}
                {{ Form::label('users_under_contriol', 'Всего на учете', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            <hr>
            <div class="form-check">
                {{Form::radio('filter_name', 'covid19',  $filter_name === 'covid19' ? true : false, ['class'=>"form-check-input"])}}
                {{ Form::label('covid19', 'Выявлен COVID-19', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            <hr>
            <div class="form-check">
                {{Form::radio('filter_name', 'covid19_under_control',  $filter_name === 'covid19_under_control' ? true : false, ['class'=>"form-check-input"])}}
                {{ Form::label('covid19_under_control', 'Выявлен COVID-19 на учете', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            <hr>
            <div class="form-check">
                {{Form::radio('filter_name', 'not_under_control', $filter_name === 'not_under_control' ? true : false, ['class'=>"form-check-input"])}}
                {{ Form::label('not_under_control', 'С симптомами', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>

            <hr>

            <div class="form-check" title="Требуется забор биоматериала">
                {{ Form::radio('filter_name', 'biomaterial_sampling_required', $filter_name === 'biomaterial_sampling_required' ? true : false, ['class'=>"form-check-input"]) }}
                {{ Form::label('biomaterial_sampling_required', 'Требуется забор биоматериала', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            {{ Form::date('biomaterial_sampling_required_date', $oldData['biomaterial_sampling_required_date'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}
            <hr>
            <div class="form-check">
                {{Form::radio('filter_name', 'contacted_by_date', $filter_name === 'contacted_by_date' ? true : false, ['class'=>"form-check-input"])}}
                {{ Form::label('isolation_conditions', 'По времени контакта', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            {{ Form::date('contacted_by_date_from', $oldData['contacted_by_date_from'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}
            {{ Form::date('contacted_by_date_to', $oldData['contacted_by_date_to'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}


            <hr>

            <div class="form-check"
                title="Сформировать отчет количество лиц находящихся на карантине на указанную дату с группировкой по полю «условия самоизоляции">
                {{ Form::radio('filter_name', 'users_under_contriol_grouped_by_isilation_condition', $filter_name === 'users_under_contriol_grouped_by_isilation_condition' ? true : false, ['class'=>"form-check-input"]) }}
                {{ Form::label('users_under_contriol_grouped_by_isilation_condition', 'Лица находящиеся на карантине в режиме самоизоляции группировка по условиям самоизоляции', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            {{ Form::date('users_under_contriol_grouped_by_isilation_condition_date', $oldData['users_under_contriol_grouped_by_isilation_condition_date'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}

            <hr>

            <div class="form-check" title="Количество человек кто где находится (на дому, в изоляторе, и т.д.)">
                {{ Form::radio('filter_name', 'users_grouped_by_isilation_condition', $filter_name === 'users_grouped_by_isilation_condition' ? true : false, ['class'=>"form-check-input"]) }}
                {{ Form::label('users_grouped_by_isilation_condition', 'Количество человек кто где находится (на дому, в изоляторе, и т.д.)', [ 'class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            {{ Form::date('users_grouped_by_isilation_condition_date', $oldData['users_grouped_by_isilation_condition_date'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}

            <hr>

            <div class="form-check" title='Сформировать список пациентов снятых с карантина'>
                {{Form::radio('filter_name', 'quaratine_end_by_date', $filter_name === 'quaratine_end_by_date' ? true : false, ['class'=>"form-check-input"])}}
                {{ Form::label('isolation_conditions', 'Cписок пациентов снятых с карантина', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            {{ Form::date('quaratine_end_by_date_from', $oldData['quaratine_end_by_date_from'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}
            {{ Form::date('quaratine_end_by_date_to', $oldData['quaratine_end_by_date_to'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}

            <hr>

            <div class="form-check" title="Сколько новых людей за дату">
                {{ Form::radio('filter_name', 'created_users_by_date', $filter_name === 'created_users_by_date' ? true : false, ['class'=>"form-check-input"]) }}
                {{ Form::label('created_users_by_date', 'Сколько новых людей за дату', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            {{ Form::date('created_users_by_date_date', $oldData['created_users_by_date_date'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}

            <hr>



            <div class="form-check" title="Сколько взяли проб на дату">
                {{ Form::radio('filter_name', 'take_by_date', $filter_name === 'take_by_date' ? true : false, ['class'=>"form-check-input"]) }}
                {{ Form::label('take_by_date', 'Сколько взяли проб на дату', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>
            {{ Form::date('take_by_date_date', $oldData['take_by_date_date'] ?? \Carbon\Carbon::now(), ['class'=>".form-control",'required'=>'']) }}

            <hr>

            <div class="form-check" title="Вывести всех с незаполненными данными">
                {{ Form::radio('filter_name', 'personal_data_is_not_filled', $filter_name === 'personal_data_is_not_filled' ? true : false, ['class'=>"form-check-input"]) }}
                {{ Form::label('personal_data_is_not_filled', 'Вывести всех с незаполненными данными', ['class'=>"form-check-label", 'style'=>'color:green;']) }}
            </div>

            <hr>
            {{ Form::close() }}
        </div>
    </div>
    <div class="col-sm" style="overflow:auto; height:100%; max-height:84vh;">
        <style>
        tr td {
            padding: 0 !important;
            margin: 0 !important;
            white-space: nowrap;
        }
        </style>
        <table id="exportabeTable" class="table table-bordered">
            @foreach ($users as $index => $user)
            {{-- $user->user_statuses->reverse() --}}
            @if ($loop->first)
            <thead style="position: sticky; top: 0; background:#eee;">
                @foreach ($user->getAttributes() as $key=> $user_prop)
                <th style="position: sticky; top: 0; background:#eee;white-space: nowrap;">
                    {{ config('constants.teble_fields.' . $key)}}
                </th>
                @endforeach
            </thead>
            <tbody>
                @endif

                <tr class='{{ $row_colors[$index] }}'>
                    @foreach ($user->getAttributes() as $key=> $user_prop)
                    <td>
                        @if ($loop->first && $key === 'id')

                        <div style='display:flex;'>
                            <a  href=" {{ route('user.show', ['user' => $user_prop])}}" 
                                type="button"
                                title="{{ $user_status_texts[$index] }}"
                                class="btn btn-primary btn-sm">
                                <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas"
                                    data-icon="info" class="svg-inline--fa fa-info fa-w-6" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512">
                                    <path fill="currentColor"
                                        d="M20 424.229h20V279.771H20c-11.046 0-20-8.954-20-20V212c0-11.046 8.954-20 20-20h112c11.046 0 20 8.954 20 20v212.229h20c11.046 0 20 8.954 20 20V492c0 11.046-8.954 20-20 20H20c-11.046 0-20-8.954-20-20v-47.771c0-11.046 8.954-20 20-20zM96 0C56.235 0 24 32.235 24 72s32.235 72 72 72 72-32.235 72-72S135.764 0 96 0z">
                                    </path>
                                </svg></a>
                            <a href="{{ route('user.edit', ['user' => $user_prop])}}" type="button"
                                class="btn btn-primary btn-sm"><svg width="15px" height="15px" aria-hidden="true"
                                    focusable="false" data-prefix="fas" data-icon="user-edit"
                                    class="svg-inline--fa fa-user-edit fa-w-20" role="img"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                    <path fill="currentColor"
                                        d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h274.9c-2.4-6.8-3.4-14-2.6-21.3l6.8-60.9 1.2-11.1 7.9-7.9 77.3-77.3c-24.5-27.7-60-45.5-99.9-45.5zm45.3 145.3l-6.8 61c-1.1 10.2 7.5 18.8 17.6 17.6l60.9-6.8 137.9-137.9-71.7-71.7-137.9 137.8zM633 268.9L595.1 231c-9.3-9.3-24.5-9.3-33.8 0l-37.8 37.8-4.1 4.1 71.8 71.7 41.8-41.8c9.3-9.4 9.3-24.5 0-33.9z">
                                    </path>
                                </svg></a>

                            <!-- a href="#" type="button" class="btn btn-primary btn-sm">
                    <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="microscope" class="svg-inline--fa fa-microscope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M160 320h12v16c0 8.84 7.16 16 16 16h40c8.84 0 16-7.16 16-16v-16h12c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32V16c0-8.84-7.16-16-16-16h-64c-8.84 0-16 7.16-16 16v16c-17.67 0-32 14.33-32 32v224c0 17.67 14.33 32 32 32zm304 128h-1.29C493.24 413.99 512 369.2 512 320c0-105.88-86.12-192-192-192v64c70.58 0 128 57.42 128 128s-57.42 128-128 128H48c-26.51 0-48 21.49-48 48 0 8.84 7.16 16 16 16h480c8.84 0 16-7.16 16-16 0-26.51-21.49-48-48-48zm-360-32h208c4.42 0 8-3.58 8-8v-16c0-4.42-3.58-8-8-8H104c-4.42 0-8 3.58-8 8v16c0 4.42 3.58 8 8 8z"></path></svg></a>

                <a href="#" type="button" class="btn btn-primary btn-sm">
                <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="thermometer" class="svg-inline--fa fa-thermometer fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M476.8 20.4c-37.5-30.7-95.5-26.3-131.9 10.2l-45.7 46 50.5 50.5c3.1 3.1 3.1 8.2 0 11.3l-11.3 11.3c-3.1 3.1-8.2 3.1-11.3 0l-50.4-50.5-45.1 45.4 50.3 50.4c3.1 3.1 3.1 8.2 0 11.3l-11.3 11.3c-3.1 3.1-8.2 3.1-11.3 0L209 167.4l-45.1 45.4L214 263c3.1 3.1 3.1 8.2 0 11.3l-11.3 11.3c-3.1 3.1-8.2 3.1-11.3 0l-50.1-50.2L96 281.1V382L7 471c-9.4 9.4-9.4 24.6 0 33.9 9.4 9.4 24.6 9.4 33.9 0l89-89h99.9L484 162.6c34.9-34.9 42.2-101.5-7.2-142.2z"></path></svg></a -->
                        </div>
                        @else
                        {{ $user_prop }}
                        @endif
                    </td>
                    @endforeach
                </tr>
                @endforeach
        </table>
    </div>
</div>
@endsection