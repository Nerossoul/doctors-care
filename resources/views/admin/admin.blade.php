@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Админ панель.</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('user.index') }}" type="button" class="btn btn-primary mb-3">Все зарегистрированые</a>
<a href="{{ route('user.create') }}"type="button" class="btn btn-primary mb-3">Добавить +</a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection