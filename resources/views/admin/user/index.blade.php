@extends('layouts.app')
@section('content')
<h2>Все пациенты</h2>
{{ Form::open(array('route' => 'search' , 'method' => 'post')) }}
<div class="row">

    <div class="col-sm col-md-2">
        <a href="{{ route('user.create') }}" type="button" class="btn btn-primary btn-sm mb-1">Добавить +</a>
        <a href="{{ route('stat') }}" type="button" class="btn btn-primary btn-sm mb-1">Статистика +</a>
    </div>

    <div class="col-sm">

        <div class="form-group">
            {{ Form::text('last_name', $search_params['last_name'] ?? null, ['class'=>"form-control", 'placeholder'=>"Поиск по Фамилии"]) }}
        </div>

    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::number('hospital_number', $search_params['hospital_number'] ?? null, ['class'=>"form-control",'placeholder'=>"Номер отделения"]) }}
        </div>
    </div>
    <div class="col-sm ">
        <div class='ml-2'>
            {{ Form::checkbox('users_under_control', null, $search_params['users_under_control'] ?? true,[ 'class'=>'form-check-input', 'style'=>'width:20px; height:20px;' ])}}
            {{ Form::label('hospital_number', ' На контроле' , [ 'class'=>'form-check-label','style'=>'font-size:18px;' ]) }}
        </div>
    </div>
    <div class="col-sm ">
        <div class="form-group">
            {{ Form::submit('Поиск' , ['class'=>'btn btn-primary']) }}
        </div>
    </div>

    <div class="col-sm">
        Всего выбрано записей: {{count($users)}}
    </div>

</div>
{{ Form::close() }}



<div class="col-sm 12 " style="overflow:auto; height:100%; max-height:84vh;">
    <style>
    tr td {
        padding: 0 !important;
        margin: 0 !important;
        white-space: nowrap;
    }
    </style>
    <table class="table table-striped table-bordered" style='padding:0 !important;'>
        @foreach ($users as $user)
        @if ($loop->first)
        <thead style="position: sticky; top: -1%; background:#eee;">
            @foreach ($user->getAttributes() as $key=> $user_prop)
            <th style="position: sticky; top: -1%; background:#eee; white-space: nowrap;">
                {{ config('constants.teble_fields.' . $key)}}
            </th>
            @endforeach
        </thead>
        <tbody>
            @endif
            <tr>
                @foreach ($user->getAttributes() as $key=> $user_prop)
                <td>
                    @if ($loop->first)
                    <div style='display:flex;'>
                        <a href="{{ route('user.show', ['user' => $user_prop])}}" type="button" class="btn btn-primary btn-sm">
                            <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas"
                                data-icon="info" class="svg-inline--fa fa-info fa-w-6" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512">
                                <path fill="currentColor"
                                    d="M20 424.229h20V279.771H20c-11.046 0-20-8.954-20-20V212c0-11.046 8.954-20 20-20h112c11.046 0 20 8.954 20 20v212.229h20c11.046 0 20 8.954 20 20V492c0 11.046-8.954 20-20 20H20c-11.046 0-20-8.954-20-20v-47.771c0-11.046 8.954-20 20-20zM96 0C56.235 0 24 32.235 24 72s32.235 72 72 72 72-32.235 72-72S135.764 0 96 0z">
                                </path>
                            </svg></a>
                        <a href="{{ route('user.edit', ['user' => $user_prop])}}" type="button" class="btn btn-primary btn-sm"><svg
                                width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas"
                                data-icon="user-edit" class="svg-inline--fa fa-user-edit fa-w-20" role="img"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                <path fill="currentColor"
                                    d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h274.9c-2.4-6.8-3.4-14-2.6-21.3l6.8-60.9 1.2-11.1 7.9-7.9 77.3-77.3c-24.5-27.7-60-45.5-99.9-45.5zm45.3 145.3l-6.8 61c-1.1 10.2 7.5 18.8 17.6 17.6l60.9-6.8 137.9-137.9-71.7-71.7-137.9 137.8zM633 268.9L595.1 231c-9.3-9.3-24.5-9.3-33.8 0l-37.8 37.8-4.1 4.1 71.8 71.7 41.8-41.8c9.3-9.4 9.3-24.5 0-33.9z">
                                </path>
                            </svg></a>

                        <!--a href="#" type="button" class="btn btn-primary btn-sm">
                    <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="microscope" class="svg-inline--fa fa-microscope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M160 320h12v16c0 8.84 7.16 16 16 16h40c8.84 0 16-7.16 16-16v-16h12c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32V16c0-8.84-7.16-16-16-16h-64c-8.84 0-16 7.16-16 16v16c-17.67 0-32 14.33-32 32v224c0 17.67 14.33 32 32 32zm304 128h-1.29C493.24 413.99 512 369.2 512 320c0-105.88-86.12-192-192-192v64c70.58 0 128 57.42 128 128s-57.42 128-128 128H48c-26.51 0-48 21.49-48 48 0 8.84 7.16 16 16 16h480c8.84 0 16-7.16 16-16 0-26.51-21.49-48-48-48zm-360-32h208c4.42 0 8-3.58 8-8v-16c0-4.42-3.58-8-8-8H104c-4.42 0-8 3.58-8 8v16c0 4.42 3.58 8 8 8z"></path></svg></a>

                <a href="#" type="button" class="btn btn-primary btn-sm">
                <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="thermometer" class="svg-inline--fa fa-thermometer fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M476.8 20.4c-37.5-30.7-95.5-26.3-131.9 10.2l-45.7 46 50.5 50.5c3.1 3.1 3.1 8.2 0 11.3l-11.3 11.3c-3.1 3.1-8.2 3.1-11.3 0l-50.4-50.5-45.1 45.4 50.3 50.4c3.1 3.1 3.1 8.2 0 11.3l-11.3 11.3c-3.1 3.1-8.2 3.1-11.3 0L209 167.4l-45.1 45.4L214 263c3.1 3.1 3.1 8.2 0 11.3l-11.3 11.3c-3.1 3.1-8.2 3.1-11.3 0l-50.1-50.2L96 281.1V382L7 471c-9.4 9.4-9.4 24.6 0 33.9 9.4 9.4 24.6 9.4 33.9 0l89-89h99.9L484 162.6c34.9-34.9 42.2-101.5-7.2-142.2z"></path></svg></a -->
                    </div>

                    @else
                    {{ $user_prop }}
                    @endif
                </td>
                @endforeach
            </tr>
            @endforeach
    </table>
</div>
@endsection