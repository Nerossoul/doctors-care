@extends('layouts.app')
@section('content')
<h1>Новый пользователь(создать)</h1>
@if (isset($error_message))
<div class="alert alert-danger" role="alert">
    {{  $error_message }}
</div>
@endif
{{ Form::open(array('route' => "user.store")) }}
<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('last_name', 'Фамилия (Обязательное поле)') }}
            {{ Form::text('last_name', $last_name ?? null, ['class'=>"form-control", 'placeholder'=>"Фамилия", 'required'=>'']) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('name', 'Имя (Обязательное поле)') }}
            {{ Form::text('name', $name ?? null, ['class'=>"form-control", 'placeholder'=>"Имя", 'required'=>'']) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('second_name', 'Отчество') }}
            {{ Form::text('second_name', $second_name ?? null, ['class'=>"form-control", 'placeholder'=>"Отчество"]) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('hospital_number', 'Номер отделения') }}
            {{ Form::number('hospital_number', $hospital_number ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm " style='max-width:240px;'>
        <div class="form-group">
            {{ Form::label('birth_day', 'Дата рождения') }}
            {{ Form::date('birth_day', $birth_day ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('phone_number', 'Телефон') }}
            {{ Form::text('phone_number', $phone_number ?? null, ['class'=>"form-control", 'placeholder'=>"Телефон"]) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('current_address', 'Текущий Адрес') }}
            {{ Form::text('current_address', $current_address ?? null, ['class'=>"form-control", 'placeholder'=>"Введите адрес"]) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('child_qty', 'Количество детей') }}
            {{ Form::number('child_qty', $child_qty ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('job', 'Место работы') }}
            {{ Form::text('job', $job ?? null, ['class'=>"form-control", 'placeholder'=>"Наименование предприятия"]) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('arrival_date', 'Дата прибытия') }}
            {{ Form::date('arrival_date', $arrival_date ?? null, ['class'=>"form-control", "id"=>"arrival_date"]) }}
        </div>
    </div>
    <div class="col-sm-1 pt-4">
        <button type="button" class="btn btn-primary btn-sm mb-1" id="addTwoWeeks">+ 14 дней</a>
    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('quarantine_end_date', 'Дата окончания карантина',['id'=>"dateDelta"]) }}
            {{ Form::date('quarantine_end_date', $quarantine_end_date ?? null, ['class'=>"form-control", 'id'=>"quarantine_end_date"]) }}
        </div>
    </div>

    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('arrival_country', 'Страна прибытия / контакт') }}
            {{ Form::text('arrival_country', $arrival_country ?? null, ['class'=>"form-control", 'placeholder'=>"arrival_country"]) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('is_need_bl', 'Нужен больничный?') }}
            {{ Form::select('is_need_bl', ['0' => 'Нет', '1' => 'Да'], $is_need_bl ?? '0', ['class'=>"form-control"]) }}
        </div>

    </div>
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('bl_receive_date', 'Дата выдачи больничного листа') }}
            {{ Form::date('bl_receive_date', $bl_receive_date ?? null, ['class'=>"form-control"]) }}
        </div>
    </div>
</div>

<div class="row border border-primary p-2">
    <div class="col-sm pr-0">
        <div class="form-group">
            {{ Form::label('take_1', 'Забор №1') }}
            {{ Form::date('take_1', $take_1 ?? null, ['class'=>"form-control"]) }}
        </div>
        <div class="form-group">
            {{ Form::label('take_1_result', 'Результат') }}
            {{ Form::select('take_1_result', ['0' => 'Отрицательный', '1' => 'Положительный'], $take_1_result ?? '0', ['class'=>"form-control"]) }}
        </div>
    </div>
    <div class="col-sm pl-0 border-right border-primary">
        <div class="form-group">
            {{ Form::label('take_1_comment', 'Коммент') }}
            {{ Form::textarea('take_1_comment', $take_1_comment ?? null, ['class'=>"form-control", 'rows'=> '5']) }}
        </div>
    </div>
    <div class="col-sm pr-0">
        <div class="form-group">
            {{ Form::label('take_2', 'Забор №2') }}
            {{ Form::date('take_2', $take_2 ?? null, ['class'=>"form-control"]) }}
        </div>
        <div class="form-group">
            {{ Form::label('take_2_result', 'Результат') }}
            {{ Form::select('take_2_result', ['0' => 'Отрицательный', '1' => 'Положительный'], $take_2_result ?? '0', ['class'=>"form-control"]) }}
        </div>
    </div>
    <div class="col-sm pl-0 border-right border-primary">
        <div class="form-group">
            {{ Form::label('take_2_comment', 'Коммент') }}
            {{ Form::textarea('take_2_comment', $take_2_comment ?? null, ['class'=>"form-control", 'rows'=> '5']) }}
        </div>

    </div>
    <div class="col-sm pr-0">
        <div class="form-group">
            {{ Form::label('take_3', 'Забор №3') }}
            {{ Form::date('take_3', $take_3 ?? null, ['class'=>"form-control"]) }}
        </div>
        <div class="form-group">
            {{ Form::label('take_3_result', 'Результат') }}
            {{ Form::select('take_3_result', ['0' => 'Отрицательный', '1' => 'Положительный'], $take_3_result ?? '0', ['class'=>"form-control"]) }}
        </div>
    </div>
    <div class="col-sm pl-0">
        <div class="form-group">
            {{ Form::label('take_3_comment', 'Коммент') }}
            {{ Form::textarea('take_3_comment', $take_3_comment ?? null, ['class'=>"form-control", 'rows'=> '5']) }}
        </div>
    </div>
</div>
<div class="form-group">
    {{ Form::label('isolation_conditions', 'Условия изоляции') }}
    {{ Form::select('isolation_conditions', config('constants.isolation_conditions'), $isolation_conditions ?? null, ['class'=>"form-control"]) }}
</div>

<div class="form-group">
    {{ Form::label('control_end_date', 'Дата окончания контроля') }}
    {{ Form::date('control_end_date', $control_end_date ?? null, ['class'=>"form-control"]) }}
</div>
<div class="form-group">
    {{ Form::label('control_result', 'Исход контроля') }}
    {{ Form::select('control_result', config('constants.control_result'), $control_result ?? null, ['class'=>"form-control"]) }}
</div>


<div class="form-group">
    {{ Form::submit('Создать') }}
</div>

{{ Form::close() }}
@endsection