@extends('layouts.app')
@section('content')
<h1>{{$last_name}} {{$name}} {{$second_name}} {{$birth_day ? "(" . $birth_day . ")" : ""}}</h1>
<div class="row">
    <div class="col-sm">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Контакты</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$phone_number}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">{{$email}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">{{$current_address}}</h6>
                <a href="{{ route('user.edit', ['user' => $id])}}" class="card-link">Изменить</a>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Доп информация</h5>
                <h6 class="card-subtitle mb-2 text-muted">Место работы: {{$job}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Нужен БЛ: {{$is_need_bl === 1? "Да" : "Нет"}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Дата выдачи БЛ: {{$bl_receive_date}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Количество детей: {{$child_qty}}</h6>
                <a href="{{ route('user.edit', ['user' => $id])}}" class="card-link">Изменить</a>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Источник</h5>

                <h6 class="card-subtitle mb-2 text-muted">Номер отделения: {{$hospital_number}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Дата прибытия: {{$arrival_date}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Страна прибытия / контакт: {{$arrival_country}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Дата окончания карантина: {{$quarantine_end_date}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Условия изоляции: {{$isolation_conditions}}</h6>

                <h6 class="card-subtitle mb-2 text-muted">Дата окончания контроля: {{$control_end_date}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Исход: {{$control_result}}</h6>
                <a href="{{ route('user.edit', ['user' => $id])}}" class="card-link">Изменить</a>
            </div>
        </div>
    </div>


</div>
<h5 class="card-title">Анализы</h5>
<div class="row">

    <div class="col-sm">
        <div class="card">
            <div class="card-body">

                <h6 class="card-subtitle mb-2 text-muted">Дата 1: {{$take_1}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Коммент 1: {{$take_1_comment}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Результат:
                    {{$take_1_result === 1? "Положительный" : "Отрицательный"}}</h6>
                <a href="{{ route('user.edit', ['user' => $id])}}" class="card-link">Изменить</a>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card">
            <div class="card-body">


                <h6 class="card-subtitle mb-2 text-muted">Дата 2: {{$take_2}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Коммент 2: {{$take_2_comment}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Результат:
                    {{$take_2_result === 1? "Положительный" : "Отрицательный"}}</h6>
                <a href="{{ route('user.edit', ['user' => $id])}}" class="card-link">Изменить</a>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card">
            <div class="card-body">
                <h6 class="card-subtitle mb-2 text-muted">Дата 3: {{$take_3}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Коммент 3: {{$take_3_comment}}</h6>
                <h6 class="card-subtitle mb-2 text-muted">Результат:
                    {{$take_3_result === 1? "Положительный" : "Отрицательный"}}</h6>
                <a href="{{ route('user.edit', ['user' => $id])}}" class="card-link">Изменить</a>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-sm">
        <div class="card border border-primary">
            <div class="card-body border-bottom border-secondary">
                {{ Form::open(array('route' => 'tmc.store')) }}
                {{ Form::hidden('user_id', $id) }}
                <div class="row">
                    <div class="col-sm col-md-3">
                        <div class="form-group">
                            {{ Form::label('diagnosis_code', 'Диагноз COVID-19') }}
                            {{ Form::select('diagnosis_code', config('constants.covid_diagnoses'), $diagnosis_code ?? null, ['class'=>"form-control"]) }}
                        </div>
                    </div>
                    <div class="col-sm col-md-4">
                        <div class="form-group">
                            {{ Form::label('ext_diagnosis_code', 'Сопутствующий диагноз') }}
                            {{ Form::text('ext_diagnosis_code', null, ['class'=>"form-control", 'placeholder'=>""]) }}
                        </div>
                    </div>
                    <div class="col-sm col-md-2">
                        <div class="form-group">
                            {{ Form::label('tmc_date', 'Дата ТМС') }}
                            {{ Form::date('tmc_date', null, ['class'=>"form-control" ]) }}
                        </div>
                    </div>
                    <div class="col-sm col-md-3">
                        <div class="form-group">
                            {{ Form::label('consultant_name', 'Консультант (обязательное поле)') }}
                            {{ Form::text('consultant_name', null, ['class'=>"form-control", 'placeholder'=>"ФИО и/или название учреждения"]) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::submit('Добавить запись', ['class'=>'btn btn-primary']) }}
                </div>
                {{ Form::close() }}
            </div>
            <table class="table">
                <tr>
                    <td>Дата время внесения</td>
                    <td>Дата ТМС</td>
                    <td>Основной диагноз</td>
                    <td>Сопутствующий диагноз</td>
                    <td>Консультант</td>
                </tr>
                @foreach ($telemedicines->reverse() as $tmc)
                <tr>
                    <td>{{$tmc->created_at}}</td>
                    <td>{{$tmc->tmc_date}}</td>
                    <td>{{$tmc->diagnosis_code}}</td>
                    <td>{{$tmc->ext_diagnosis_code}}</td>
                    <td>{{$tmc->consultant_name}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-sm">
        <div class="card border border-primary">
            <div class="card-body border-bottom border-secondary">
                <h3 class="card-subtitle mb-2 text-muted">Таблица состояний</h3>

                {{ Form::open(array('route' => 'status.store')) }}
                <div class="row">
                    <div class="col-sm col-md-4">

                        {{ Form::hidden('user_id', $id) }}

                        <div class="form-group">
                            {{ Form::label('date', 'Дата') }}
                            {{ Form::date('date', \Carbon\Carbon::now(), ['class'=>"form-control", 'min'=> \Carbon\Carbon::now()->add(-1, 'day') ]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('temperature', 'Температура') }}
                            {{ Form::number('temperature', null, ['class'=>"form-control", 'min'=>"35" ,'max'=>"42", 'step'=>"0.1"]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('cough_pain_throat', 'Есть кашель или боль в горле') }}
                            {{ Form::select('cough_pain_throat', [''=>'нет данных','0' => 'Нет', '1' => 'Да'], null, ['class'=>"form-control"]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('short_wind', 'Затруднено дыхание') }}
                            {{ Form::select('short_wind', [''=>'нет данных', '0' => 'Нет', '1' => 'Да'], null, ['class'=>"form-control"]) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('need_consult', 'Есть иные симптомы или требуется консультация врача') }}
                            {{ Form::select('need_consult', [''=>'нет данных','0' => 'Нет', '1' => 'Да'], null, ['class'=>"form-control"]) }}
                        </div>
                    </div>
                    <div class="col-sm col-md-8">
                        <div class="form-group">
                            {{  Form::textarea('comment', null, ['style'=>'width:100%', 'placeholder'=>'Иное / Комментарий'] )  }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::submit('Добавить запись') }}
                </div>
                {{ Form::close() }}

            </div>
            <table class="table">

                <tr>
                    <td>Дата время внесения</td>
                    <td>Статус за дату</td>
                    <td>Температура</td>
                    <td>Кашель или боль в горле</td>
                    <td>Затруднено дыхание</td>
                    <td>Иные симптомы</td>
                    <td>Комментарий</td>
                </tr>
                @foreach ($statuses->reverse() as $status)
                <tr>
                    <td>{{$status->created_at}}</td>
                    <td>{{$status->date}}</td>
                    <td>{{$status->temperature}}</td>
                    <td>{{$status->cough_pain_throat}}</td>
                    <td>{{$status->short_wind}}</td>
                    <td>{{$status->need_consult}}</td>
                    <td>{{$status->comment}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

<script>

</script>

@endsection