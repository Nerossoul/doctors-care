<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('second_name')->after('name')->nullable();
            $table->string('last_name')->after('id')->nullable();
            $table->date('birth_day')->after('second_name')->nullable();
            $table->string('phone_number', 500)->after('email')->unique()->nullable();
            $table->date('arrival_date')->after('phone_number')->nullable();
            $table->string('current_address', 500)->after('arrival_date')->nullable();
            $table->smallInteger('hospital_number')->after('current_address')->nullable();
            $table->string('arrival_country', 500)->after('hospital_number')->nullable();
            $table->date('quarantine_end_date')->after('arrival_country')->nullable();
            $table->date('take_1')->after('quarantine_end_date')->nullable();
            $table->date('take_2')->after('take_1')->nullable();
            $table->date('take_3')->after('take_2')->nullable();
            $table->string('take_1_comment', 500)->after('take_1')->nullable();
            $table->string('take_2_comment', 500)->after('take_2')->nullable();
            $table->string('take_3_comment', 500)->after('take_3')->nullable();
            $table->boolean('take_1_result')->after('take_1_comment')->default(0);
            $table->boolean('take_2_result')->after('take_2_comment')->default(0);
            $table->boolean('take_3_result')->after('take_3_comment')->default(0);
            $table->boolean('is_need_bl')->after('take_3_result')->default(0);
            $table->date('bl_receive_date')->after('is_need_bl')->nullable();
            $table->integer('child_qty')->after('bl_receive_date')->default(0);
            $table->string('job', 500)->after('child_qty')->nullable();
            $table->date('control_end_date')->after('job')->nullable();
            $table->string('control_result', 500)->after('control_end_date')->nullable();
            $table->string('isolation_conditions', 500)->after('control_result')->nullable();
            $table->json('ext_information')->after('isolation_conditions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('second_name');
            $table->dropColumn('last_name');
            $table->dropColumn('birth_day');
            $table->dropColumn('phone_number');
            $table->dropColumn('arrival_date');
            $table->dropColumn('current_address');
            $table->dropColumn('hospital_number');
            $table->dropColumn('arrival_country');
            $table->dropColumn('quarantine_end_date');
            $table->dropColumn('take_1');
            $table->dropColumn('take_2');
            $table->dropColumn('take_3');
            $table->dropColumn('take_1_comment');
            $table->dropColumn('take_2_comment');
            $table->dropColumn('take_3_comment');
            $table->dropColumn('take_1_result');
            $table->dropColumn('take_2_result');
            $table->dropColumn('take_3_result');
            $table->dropColumn('is_need_bl');
            $table->dropColumn('bl_receive_date');
            $table->dropColumn('child_qty');
            $table->dropColumn('job');
            $table->dropColumn('control_end_date');
            $table->dropColumn('control_result');
            $table->dropColumn('isolation_conditions');
            $table->dropColumn('ext_information');

        });
    }
}