<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTelemedicines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telemedicines', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->date('tmc_date')->nullable();
            $table->string('diagnosis_code', 1000)->nullable();
            $table->string('ext_diagnosis_code', 10000)->nullable();
            $table->string('consultant_name', 1000)->nullable();
            $table->json('ext_information')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telemedicines');
    }
}
