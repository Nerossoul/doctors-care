<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statuses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->date('date')->nullable();
            $table->float('temperature', 3, 1)->nullable();
            $table->boolean('cough_pain_throat')->nullable();
            $table->boolean('short_wind')->nullable();
            $table->boolean('need_consult')->nullable();
            $table->foreignId('added_by')->nullable();
            $table->string('comment', 1000)->nullable();
            $table->json('ext_information')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_statuses');
    }
}
